﻿# Parameters
$Root = Get-Location
$SourceFile = "sample_file.xlsx"
$InputFile = "$Root\$SourceFile"
$Path = $InputFile


# Just for logging, you can ignore this block
# ---------------------------
Function Write-Log(){
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [String]$Text,

        [Switch]$NoNewLine,
        
        [Parameter(Mandatory=$False)]
        [String]$TextColor="White"
    )
    $date = $NULL
    if (!($PrevNoNewLine)){
        $date = "[$(Get-Date -Format "mm-dd-yyyy: HH:mm:ss")] "
        Write-Host $date -NoNewline
    }
    If ($NoNewLine){
        Write-Host $Text -ForegroundColor $TextColor -NoNewline
        Set-Variable -Name PrevNoNewLine -Value $True -Scope Global
    }
    else{
         Write-Host $Text -ForegroundColor $TextColor
         Set-Variable -Name PrevNoNewLine -Value $False -Scope Global
    }
}
# ---------------------------

Function Download-File(){
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [String]$URL,
        
        [Parameter(Mandatory=$True)]
        [String]$OutputLocation
    )
    If (!(Test-Path $(Split-Path($OutputLocation) -Parent))){
        Write-Log "Output folder will be created since it does not exists yet."
        New-Item $(Split-Path($OutputLocation) -Parent) -ItemType Directory -Force | Out-Null
    }
    Write-Log "Downloading file now..."
    $download = Invoke-WebRequest -Uri $URL -OutFile $OutputLocation
}


# =========================
# Script starts here...
# =========================

# 1. Entry point
# ---------------------------
Write-Log "================================"
Write-Log "Script started."

# 2. Check if input file is existing
# ---------------------------
If (!(Test-Path $Path)){
    Write-Log "File does not exist." -TextColor "Red"
    Exit(1)
}

# 3. Read the excel file
# ---------------------------
try{
    Write-Log "Reading file: " -NoNewLine
    Write-Log "[$Path]" -TextColor "Yellow"
    $Content = Import-Excel -Path $Path -ErrorAction Stop
}
catch{
    Write-Log $Error[0].Exception.Message -TextColor "Red"
    Exit(1)
}

# 4. Process each row
# ---------------------------
$Num = 0
$SummaryOutput = @()
foreach ($row in $Content){
    try{
        $Num = $Num + 1
        $current = "[$Num of $($Content.Count)]"
        Write-Log "$current Downloading file of Developer: " -NoNewLine
        Write-Log $row.Developer -TextColor "Yellow"
        Write-Log "$current Saving to: " -NoNewLine
        Write-Log $row.'File Location' -TextColor "Yellow"
        $OutFile = "$($row.'File Location')$($row.'File Name')"
        Download-File -URL $($row.'URL to Download') -OutputLocation $OutFile
        if (Test-Path $OutFile){
            Write-Log "File downloaded." -TextColor "Green"
            $Status = "Success"
            $Remarks = $OutFile
        }
        else{
            $Status = "Failed"
            $Remarks = "Encountered some error."
        }
    }
    catch{
        Write-Log "Error encountered: " -NoNewLine
        Write-Log $Error[0].Exception.Message -TextColor "Red"
        $Status = "Failed"
        $Remarks = $Error[0].Exception.Message
    }
    finally{
        $Data = "" | Select Developer, System, FileLocation, DownloadStatus, Remarks, URL
        $Data.Developer = $row.Developer
        $Data.System = $row.System
        $Data.FileLocation = $OutFile
        $Data.DownloadStatus = $Status
        $Data.Remarks = $Remarks
        $Data.URL = $row.'URL to Download'
        $SummaryOutput += $Data
    }
}
Write-Log "================================"

# 5. Out summary result to csv
# -----------------------------
try{
    $SummaryOutputPath = "$Root\summary_output.csv"
    Write-Log "Creating summary output: " -NoNewLine
    Write-Log $SummaryOutputPath -TextColor "Yellow"
    $SummaryOutput | Export-Csv $SummaryOutputPath -NoTypeInformation -Force
}
catch{
    Write-Log "WARNING: $($Error[0].Exception.Message)" -TextColor "Magenta"
    Exit(1)
}

# 6. Script completed
# -----------------------------
Write-Log "Script completed. Please check the summary output file."
Write-Log "================================"




