# Powershell downloader
---
*`This is a draft`*

1. The PowerShel script `downloader.ps1` get the list of `data` from the Excel file `sample_file`.
2. Parses the `data` to get the `filename`, `output location` and `url link` columns to be used as parameters in the PowerShell script routine.
3. Generates the `summary_output.csv` that contains the details of the PowerShell script execution. 